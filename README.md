# gitpod - [gitpod.io](www.gitpod.io)
This repo describes how you can create learning environements with gitpod.

[Currently i'm testing an Flask environements where u can learn how to install Flask and some basics.](https://gitlab.com/omeyer99/flask-learning-env)

**Basics:**

Gitpod is an open source platform for automated and ready-to-code development environments. It enables developers to describe their dev environment as code and start instant and fresh development environments for each new task directly from your browser. Gitpod seamlessly integrates in your workflow and works with all major git hosting platforms including GitHub, GitLab and Bitbucket. It understands the context you are in and adjusts your dev environment accordingly. For example, if you create a Gitpod workspace from a Pull or Merge Request, Gitpod will open a fully-initialized dev environment in code-review mode. 

At its core Gitpod relies on a client-server architecture where the client can either be:
- Any device with a browser and internet connection (Gitpod works with Chrome, Firefox, Safari, Edge and other Chromium-based browsers)
- Your local machine via local VS Code, IntelliJ or simply your shell/terminal where you SSH into your Gitpod workspace (expected to be released in early Q1/2021)

Server-side Gitpod is a Kubernetes application that understands the context from GitLab, GitHub and Bitbucket and spins up containerized dev environments. Under the hood is a customizable Linux container, which we call your workspace. 
A workspace comprises your whole development environment and gives you similar capabilities to a Linux machine. Compared to the latter it is however pre-configured and optimized for your individual development workflow. Each workspace includes:
Your source code
- A shell with root / sudo capabilities
- Your IDE of choice* - currently this is VS Code or Theia
- Your personal IDE extensions, themes, editor prefs
- Full Docker support `sudo docker-up`  | creating a Docker host in a hosted environement isn't possible for security reason (`docker daemon isn't running`). Nevertheless it should be possible on a selfhosted gitpod-server

To install self-hosted Gitpod the following site should guide you through the installatin process: [Gitpod](https://www.gitpod.io/docs/self-hosted/0.4.0/install/install-on-kubernetes/) 

---

## Creating your first workspace - [URL](https://www.gitpod.io/docs/configuration)

1. **Create .gitpod.yml**

The .gitpod.yml contains everything that describes your dev environment as code. Most importantly it contains a list of commands that should be executed when a dev environment is prebuilt and when it is started.

2. **Change Base Image**

The .gitpod.yml optionally references a Docker image or a Docker file, which is used as the container for your dev environment. This is where you install all the tools (such as runtimes, compilers, databases, etc.) that you need during development. 

3. **Update Readme**

In case you want to tell your team or community about the automated dev environments you can add a badge to your README.md.

--- 

## Creating Learning Environments - [learnpack](https://learnpack.co)

Learnpack is a useful package to build tutorials. The documentation is more or less available, but all in all it's a quiet useful tool.
The next steps will guide you througgh the progress of creating a tutorial.

1. `learnpack init` creates a new learning package
2. `learnpack start` runs a small server with all excercise instructions, which are located in .learn

If you want to deploy learning environments it's quiet useful to update the `.gitpod.Dockerfile` and the `.gitpod.yml` allowing learnpack to be installed and run in pre-installation. The configuration should look like this:

```dockerfile
RUN npm install -g learnpack
```

```yaml
tasks:
  - command: learnpack start -g incremental
``` 
