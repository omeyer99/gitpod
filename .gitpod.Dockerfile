# a good starting point for creating a custom Dockerfile is the 'gitpod/workspace-full' image, because it already contains a lot of necessary tools.
FROM gitpod/workspace-full

# RUN commands. for example 'apt update && apt upgrade -y'
RUN sudo apt update && sudo apt upgrade -y
